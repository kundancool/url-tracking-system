# Setting up project
-

* Setup the project by installing dependencies.

```
$ composer install
```

* Configure databse credentials in ```.env``` file

* Prepare database for the application and run the migrations for application.

```
$ php artisan migrate
```

* Start the web server for the project using artisan serve or _valet to link as a .test domain_

```
$ php artisan serve
```

* Optional - seed database with demo data

```
$ php artisan db:seed --class=HashedUrlSeeder
```
