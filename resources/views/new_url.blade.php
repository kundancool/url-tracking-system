<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <h2 class="font-bold uppercase pb-5 align-middle">
                        Add URL
                    </h2>
                    <div class="container">
        				<x-auth-validation-errors class="mb-4" :errors="$errors" />
                        <form action="{{ route('store_track_url') }}" method="post">
                        	@csrf
                            <div class="mb-4">
                                <label class="block text-gray-700 text-sm font-semibold mb-2" htmlfor="email">
                                    URL
                                </label>
                                <input class="text-sm appearance-none rounded w-full py-2 px-3 text-gray-400 bg-gray-200 leading-tight focus:outline-none focus:shadow-outline h-10" id="url" name="url" placeholder="URL to Track" type="text" required="" />
                            </div>
                            <div class="flex w-full mt-8">
                                <button class="w-full bg-gray-800 hover:bg-grey-900 text-white text-sm py-2 px-4 font-semibold rounded focus:outline-none focus:shadow-outline h-10" type="submit">
                                    Add
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
