<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <h2 class="font-bold uppercase pb-2 h-12 align-middle">
                        Hashed URLs
                        <a href="{{ route('create_track_url') }}" class="border px-4 py-1 float-right rounded uppercase bg-blue-700 text-white">+ Add</a>
                    </h2>
                    <div class="container">
                        <table class="table-auto">
                            <tr>
                                <th class="bg-gray-100 border px-4 py-2">ID</th>
                                <th class="bg-gray-100 border px-4 py-2">Hash</th>
                                <th class="bg-gray-100 border px-4 py-2">URL</th>
                                <th class="bg-gray-100 border px-4 py-2">Tracking URL</th>
                                <th class="bg-gray-100 border px-4 py-2">Hits</th>
                                <th class="bg-gray-100 border px-4 py-2">Created</th>
                            </tr>
                            @foreach ($hashed_urls as $url)
                            <tr>
                                <td class="border text-sm px-2 py-2 text-center">{{ $url->id }}</td>
                                <td class="border text-sm px-2 py-2 text-center">{{ $url->hash }}</td>
                                <td class="border text-sm px-2 py-2 break-all underline">
                                    <a href="{{ $url->url.'?'.http_build_query(json_decode($url->params, true)) }}" target="_BLANK">{{ $url->url.'?'.http_build_query(json_decode($url->params, true)) }}</a>
                                </td>
                                <td class="border text-sm px-2 py-2 text-center underline"><a href="{{ route('track_url', ['urlHash' => $url->hash]) }}" target="_BLANK">{{ route('track_url', ['urlHash' => $url->hash]) }}</a></td>
                                <td class="border px-2 py-2 text-center text-xl">{{ $url->hits }}</td>
                                <td class="border text-sm px-2 py-2 w-56">{{ $url->created_at }}</td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                    <div class="px-2 pt-4">
                        {{ $hashed_urls->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
