## Assumptions

* URLs with UTM Tracking will be passed to hashing system to generated hashed URL
* Generated hashed URL does not need to have all UTM tracking parameters in URL
* Tracking URL can have privacy toggle argument which will bypass tracking of URL
* No special URL format has been mentioned as requirement so simple __GET__ request type URL is my preferred choice.
