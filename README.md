# Readme
This is a sample project which has a basic authentication system using email and password for authentication. 

## Purpose
This system is designed to store links and create a hashed URL to record no of hits on that URL and to create a simple URL that can be shared easily with others without the URL being modified by any kind of formatters.

## Setup
Please follow ``setup.md`` for instructions on how to setup the project.

