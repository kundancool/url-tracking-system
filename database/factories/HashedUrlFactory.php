<?php

namespace Database\Factories;

use App\Models\HashedUrl;
use Illuminate\Database\Eloquent\Factories\Factory;

class HashedUrlFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = HashedUrl::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $url = 'https://'.$this->faker->safeEmailDomain.'/'.$this->faker->slug;
        $params = [
            'utm_source' => $this->faker->word(1),
            'utm_medium' => $this->faker->word(1),
            'utm_campaign' => $this->faker->sentence(3),
            'utm_term' => $this->faker->word(1),
            'utm_content' => $this->faker->sentence(5),
        ];

        return [
            'hash' => md5($url.'?'.http_build_query($params)),
            'url' => $url,
            'params' => json_encode($params),
        ];
    }
}
