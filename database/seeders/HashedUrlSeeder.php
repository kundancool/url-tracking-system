<?php

namespace Database\Seeders;

use App\Models\HashedUrl;
use Illuminate\Database\Seeder;

class HashedUrlSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        HashedUrl::factory()->times(10)->create();
    }
}
