<?php

namespace App\Http\Controllers;

use App\Models\HashedUrl;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class HashedUrlController extends Controller
{
    /**
     * Dashboard.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function dashboard(Request $request)
    {
        $size = ($request->has('size')) ? intval($request->size) : 10;
        $hashed_urls = HashedUrl::orderBy('id', 'DESC')->paginate($size);

        return view('dashboard')->with('hashed_urls', $hashed_urls);
    }

    /**
     * Update hits on hashrd url.
     *
     * @return \Illuminate\Http\Response
     */
    public function trackUrl(Request $request, string $urlHash)
    {
        $hashedurl = HashedUrl::where('hash', $urlHash)->first();

        if ($request->has('notrack') && false == $request->notrack) {
            return response()->noContent();
        }

        if (false == empty($hashedurl)) {
            $hashedurl->increment('hits');

            return redirect()->away($hashedurl->url.'?'.http_build_query(json_decode($hashedurl->params, true)));
        }

        return response()->noContent();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('new_url');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'url' => 'required|url|max:2048',
        ]);

        $hashedurl = HashedUrl::where('hash', md5($request->url))->first();
        if (empty($hashedurl)) {
            $params = [];
            $url = explode('?', $request->url, 2);
            if (false == empty($url[1])) {
                parse_str($url[1], $params);
            }
            $hashedurl = new HashedUrl();
            // $hashedurl->hash = Str::orderedUuid();
            $hashedurl->hash = md5($request->url);
            $hashedurl->url = $url[0];
            $hashedurl->params = json_encode($params);
            $hashedurl->save();
        }

        return redirect()->route('dashboard');

        // return response()->json([
        //     'message' => 'Tracking URL generated',
        //     'data' => [
        //         'url' => $request->url,
        //         'tracking_url' => route('track_url', ['urlHash' => $hashedurl->hash]),
        //         'hits' => $hashedurl->hits,
        //     ],
        // ]);
    }
}
