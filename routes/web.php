<?php

use App\Http\Controllers\HashedUrlController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('dashboard');
});

require __DIR__.'/auth.php';

Route::group(['middleware' => 'auth'], function () {
    Route::get('/dashboard', [HashedUrlController::class, 'dashboard'])->name('dashboard');
    Route::get('/new', [HashedUrlController::class, 'create'])->name('create_track_url');
    Route::post('/t', [HashedUrlController::class, 'store'])->name('store_track_url');
});

Route::get('/t/{urlHash}', [HashedUrlController::class, 'trackUrl'])->name('track_url');
